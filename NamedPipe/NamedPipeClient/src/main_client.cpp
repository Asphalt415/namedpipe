#include <Windows.h>
#include <iostream>

constexpr int BUF_SIZE = 1024;

int main()
{
	HANDLE hPipe;
	char Buffer[BUF_SIZE];
	DWORD dwWrt;

	std::cout << "Try to connect Server." << std::endl;

	hPipe = CreateFile("\\\\.\\pipe\\MyPipe", GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (hPipe == INVALID_HANDLE_VALUE)
	{
		std::cout << "Failed to open the appointed named pipe. Error code: " << GetLastError() << std::endl;
		system("pause");
		return -1;
	}
	else
	{
		while (1)
		{
			gets_s(Buffer);
			if (WriteFile(hPipe, Buffer, strlen(Buffer), &dwWrt, nullptr))
			{
				std::cout << dwWrt << "bytes sent" << std::endl;
			}
			else
			{
				std::cout << "Failed to send message. Error code: " << GetLastError() << std::endl;
				CloseHandle(hPipe);
				std::cin.get();
				return -1;

			}
		}
	}

	std::cin.get();
	return 0;
}