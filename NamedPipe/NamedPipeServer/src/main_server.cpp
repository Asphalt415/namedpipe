#include <Windows.h>
#include <iostream>

constexpr int BUF_SIZE = 1024;

int main()
{
	HANDLE hPipe;
	char buf_msg[BUF_SIZE];
	DWORD dwRcv;

	hPipe = CreateNamedPipe(L"\\\\.\\pipe\\MyPipe", PIPE_ACCESS_INBOUND, PIPE_READMODE_BYTE | PIPE_WAIT | PIPE_ACCEPT_REMOTE_CLIENTS, PIPE_UNLIMITED_INSTANCES, BUF_SIZE, BUF_SIZE, 0, NULL);
	
	if (hPipe == INVALID_HANDLE_VALUE)
	{
		std::cout << "Failed to create named pipe. Error code: " << GetLastError() << std::endl;
		std::cin.get();
		return -1;
	}
	else
	{
		std::cout << "Named pipe created successfully." << std::endl;
	}


	if (ConnectNamedPipe(hPipe, nullptr))
	{
		std::cout << "Client connected" << std::endl;
		ZeroMemory(buf_msg, BUF_SIZE);
		while (1)
		{
			if (ReadFile(hPipe, buf_msg, BUF_SIZE, &dwRcv, nullptr))
			{
				std::cout << "Max received bytes:" << BUF_SIZE << ". Actually received bytes:" << dwRcv << std::endl;
			}
			else
			{
				std::cout << "Failed to receive message. Error code: " << GetLastError() << std::endl;
				CloseHandle(hPipe);
				std::cin.get();
				return 0;
			}

		}
	}
	
	CloseHandle(hPipe);
	std::cin.get();
	return 0;
}